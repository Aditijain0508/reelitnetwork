//
//  ViewController.swift
//  Network
//
//  Created by Aditi Jain 3 on 28/05/22.
//

import UIKit

class ViewController: UIViewController {
    let networkManager = NetworkManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        networkManager.getPosts() { result, error in
            print(result?.count ?? 0 as Int)
        }
        networkManager.addPost() { result, error in
            print(result as Any)
        }

    }


}

